import { initializeApp } from "firebase/app";
import { getFirestore, collection, addDoc } from "firebase/firestore";


// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyA4rVG8wieYs-cekbhh2aIRRyT2BhwKhUM",
  authDomain: "chat-84678.firebaseapp.com",
  projectId: "chat-84678",
  storageBucket: "chat-84678.appspot.com",
  messagingSenderId: "686552006622",
  appId: "1:686552006622:web:5b83df7389068acd2be74f",
  measurementId: "G-RTGNBVNBL7"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app); // Access Firestore inside the initialization
const usersCollection = collection(db, 'users'); // Здесь 'users' - имя вашей коллекции
// Функция build
const build = () => {
  console.log("Приложение успешно построено с использованием конфигурации Firebase!");
};

// Вызываем функцию build
build();

export { firebaseConfig, db }; // Экспорт обоих объектов