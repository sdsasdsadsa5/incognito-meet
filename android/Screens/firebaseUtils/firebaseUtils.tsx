// firebaseUtils.js

import { getFirestore, doc, setDoc } from 'firebase/firestore';
import { getAuth } from 'firebase/auth';

export const createUserDocument = async () => {
  const auth = getAuth();
  const currentUser = auth.currentUser;
  
  if (currentUser) {
    const db = getFirestore();
    const userRef = doc(db, 'users', currentUser.uid);
    
    // Установка данных пользователя в документ
    await setDoc(userRef, {
      username: 'Имя пользователя',
      email: currentUser.email,
      // Другие данные пользователя, если необходимо
    });
  }
};
