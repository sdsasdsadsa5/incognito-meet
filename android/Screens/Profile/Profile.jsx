import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, TextInput, ActivityIndicator } from 'react-native';
import { collection, query, getDocs } from 'firebase/firestore';
import { db } from '../../../Config/firebasefig';

export default class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentUser: null,
      newDescription: '',
      status: '',
      isLoading: true,
      isOnline: false,
      friends: [],
      groups: [],
      showPersonalInfo: false,
    };
  }

  componentDidMount() {
    this.loadUserData();
  }

  async loadUserData() {
    try {
      const querySnapshot = await getDocs(query(collection(db, 'users')));
      querySnapshot.forEach((doc) => {
        this.setState({ currentUser: doc.data(), isLoading: false });
      });
    } catch (error) {
      console.error('Error loading user data: ', error);
      this.setState({ isLoading: false });
    }
  }

  handleDescriptionChange = (text) => {
    this.setState({ newDescription: text });
  };

  handleStatusChange = (text) => {
    this.setState({ status: text });
  };

  render() {
    const { currentUser, newDescription, status, isLoading, isOnline, friends, groups, showPersonalInfo } = this.state;
    return (
      <View style={styles.container}>
        {isLoading ? (
          <ActivityIndicator size="large" color="#5181b8" />
        ) : (
          <>
            <Text style={styles.header}>Edit Profile</Text>
            <View style={styles.profileHeader}>
              <View style={styles.avatarContainer}>
                <Image source={{ uri: currentUser.avatar }} style={styles.avatar} />
                {isOnline && <Text style={styles.onlineIndicator}>Online</Text>}
              </View>
              <View style={styles.profileInfo}>
                <Text style={styles.label}>Current Username:</Text>
                <Text style={styles.currentData}>{currentUser.username}</Text>
                <Text style={styles.label}>Current Description:</Text>
                <TextInput
                  style={styles.input}
                  placeholder="Enter new description"
                  onChangeText={this.handleDescriptionChange}
                  value={newDescription}
                />
              </View>
            </View>
            <View style={styles.additionalInfoContainer}>
              <TextInput
                style={styles.statusInput}
                placeholder="Set Status"
                onChangeText={this.handleStatusChange}
                value={status}
              />
              <View style={styles.friendsInfo}>
                <Text style={styles.label}>Friends:</Text>
                {friends.map((friend, index) => (
                  <Text key={index} style={styles.friendName}>{friend.name}</Text>
                ))}
              </View>
              <View style={styles.groupsContainer}>
                <Text style={styles.label}>Groups:</Text>
                {groups.map((group, index) => (
                  <Text key={index} style={styles.groupName}>{group.name}</Text>
                ))}
              </View>
              <TouchableOpacity style={styles.showPersonalInfoButton} onPress={() => this.setState({ showPersonalInfo: !showPersonalInfo })}>
                <Text style={styles.buttonText}>{showPersonalInfo ? 'Hide Personal Info' : 'Show Personal Info'}</Text>
              </TouchableOpacity>
              {showPersonalInfo && (
                <View style={styles.personalInfoContainer}>
                  {/* Add personal information here */}
                </View>
              )}
            </View>
          </>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    padding: 20,
    backgroundColor: '#f7f7f7',
  },
  header: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
    color: '#5181b8',
  },
  profileHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  avatarContainer: {
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 75,
    padding: 3,
    marginRight: 20,
  },
  avatar: {
    width: 150,
    height: 150,
    borderRadius: 75,
  },
  onlineIndicator: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    backgroundColor: '#5cb85c',
    paddingVertical: 3,
    paddingHorizontal: 8,
    borderRadius: 10,
    color: '#fff',
    fontSize: 12,
    fontWeight: 'bold',
  },
  profileInfo: {
    flex: 1,
  },
  label: {
    fontSize: 16,
    marginBottom: 5,
    color: '#5181b8',
  },
  currentData: {
    marginBottom: 15,
    color: '#333',
  },
  input: {
    width: '100%',
    height: 40,
    borderColor: '#ccc',
    borderWidth: 1,
    borderRadius: 5,
    marginBottom: 20,
    paddingHorizontal: 10,
    fontSize: 16,
  },
  editButton: {
    backgroundColor: '#5181b8',
    paddingVertical: 12,
    paddingHorizontal: 20,
    borderRadius: 20,
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
  additionalInfoContainer: {
    width: '100%',
    marginTop: 20,
  },
  statusInput: {
    width: '100%',
    height: 40,
    borderColor: '#ccc',
    borderWidth: 1,
    borderRadius: 5,
    marginBottom: 20,
    paddingHorizontal: 10,
    fontSize: 16,
  },
  friendsInfo: {
    marginBottom: 20,
  },
  friendName: {
    fontSize: 16,
    marginBottom: 5,
  },
  groupsContainer: {
    marginBottom: 20,
  },
  groupName: {
    fontSize: 16,
    marginBottom: 5,
  },
  showPersonalInfoButton: {
    backgroundColor: '#5181b8',
    paddingVertical: 12,
    paddingHorizontal: 20,
    borderRadius: 20,
    alignSelf: 'center',
  },
  personalInfoContainer: {
    marginTop: 20,
  },
});
