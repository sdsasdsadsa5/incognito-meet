import React, { useEffect, useState, useCallback } from 'react';
import { View, Text, Image, ScrollView, TouchableOpacity, StyleSheet, TextInput } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { getFirestore, collection, getDocs } from 'firebase/firestore';

interface User {
  id: string;
  name: string;
}
const result: string[] = [];
const Home = () => {
  const navigation = useNavigation();
  const [searchTerm, setSearchTerm] = useState('');
  const [users, setUsers] = useState<User[]>([]);

  const fetchUsers = useCallback(async () => {
    try {
      const db = getFirestore();
      const usersCollection = collection(db, 'users');
      const usersSnapshot = await getDocs(usersCollection);
      const usersData = usersSnapshot.docs.map(doc => ({
        id: doc.id,
        name: doc.data().username || doc.data().email
      }));

      setUsers(usersData);
    } catch (error) {
      console.error('Error fetching users:', error);
    }
  }, []);

  useEffect(() => {
    fetchUsers();
  }, [fetchUsers]);

  const filteredUsers = users.filter(user =>
    user.name && user.name.toLowerCase().includes(searchTerm.toLowerCase())
  );
  
  

  return (
    <View style={styles.container}>
      <View style={styles.searchContainer}>
        <Image source={require('../../img/search.png')} style={styles.searchIcon} />
        <TextInput
          style={styles.searchInput}
          placeholder="Search users..."
          onChangeText={text => setSearchTerm(text)}
          value={searchTerm}
        />
      </View>
      <ScrollView contentContainerStyle={styles.scrollViewContainer}>
        {filteredUsers.map(user => (
          <TouchableOpacity
            key={user.id}
            style={styles.userCard}
            onPress={() => navigation.navigate('ProfileScreen', { userName: user.name })}
          >
            <Image source={require('../../img/profile.png')} style={styles.userImage} />
            <Text style={styles.userName}>{user.name}</Text>
          </TouchableOpacity>
        ))}
      </ScrollView>
      <View style={styles.bottomMenu}>
        <TouchableOpacity style={styles.menuItem} onPress={() => navigation.navigate('Home' as never)}>
          <Image source={require('../../img/home1.png')} style={styles.icon} />
          <Text style={styles.menuText}>Главное</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.menuItem} onPress={() => navigation.navigate('Chat' as never)}>
          <Image source={require('../../img/chat1.png')} style={styles.icon} />
          <Text style={styles.menuText}>Сообщения</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.menuItem} onPress={() => navigation.navigate('AddFriends' as never)}>
          <Image source={require('../../img/friend1.png')} style={styles.icon} />
          <Text style={styles.menuText}>Друзья</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.menuItem} onPress={() => navigation.navigate('Settings' as never)}>
          <Image source={require('../../img/Settings1.png')} style={styles.icon} />
          <Text style={styles.menuText}>Настройки</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.menuItem} onPress={() => navigation.navigate('Ai' as never)}>
          <Image source={require('../../img/Ai.png')} style={styles.icon} />
          <Text style={styles.menuText}>Ai</Text>
        </TouchableOpacity>
        
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#480066',
  },
  searchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    borderRadius: 8,
    margin: 10,
    paddingHorizontal: 10,
  },
  searchIcon: {
    width: 24,
    height: 24,
    marginRight: 10,
  },
  searchInput: {
    flex: 1,
    height: 40,
    fontSize: 16,
  },
  scrollViewContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    paddingVertical: 10,
  },
  userCard: {
    width: '45%',
    backgroundColor: '#FFFFFF',
    borderRadius: 8,
    alignItems: 'center',
    marginBottom: 10,
    elevation: 2,
  },
  userImage: {
    width: 100,
    height: 100,
    borderRadius: 50,
    marginVertical: 10,
  },
  userName: {
    color: '#000000',
    fontSize: 16,
    marginBottom: 10,
  },
  bottomMenu: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    height: 60,
    borderTopWidth: 1,
    borderTopColor: '#C8C7CC',
    backgroundColor: '#FFFFFF',
  },
  menuItem: {
    flex: 1,
    alignItems: 'center',
  },
  menuText: {
    color: '#007AFF',
  },
  icon: {
    width: 24,
    height: 24,
    marginBottom: 4,
  },
});

export default Home;
