import React, { useState } from 'react';
import { View, Text, ScrollView, TouchableOpacity, Image, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { getFirestore, collection, getDocs, doc, setDoc } from 'firebase/firestore';

interface User {
  id: string;
  name: string;
}

const AddFriendsScreen = () => {
  const navigation = useNavigation();
  const [users, setUsers] = useState<User[]>([]);
  const [isLoading, setIsLoading] = useState(false);
  const [isFetched, setIsFetched] = useState(false);

  const fetchUsers = async () => {
    try {
      setIsLoading(true);
      const db = getFirestore();
      const usersCollection = collection(db, 'users');
      const usersSnapshot = await getDocs(usersCollection);
      const usersData = usersSnapshot.docs.map(doc => ({
        id: doc.id,
        name: doc.data().username || doc.data().email
      }));
      setUsers(usersData);
      setIsFetched(true); // Устанавливаем флаг, чтобы показать, что данные загружены
    } catch (error) {
      console.error('Error fetching users:', error);
    } finally {
      setIsLoading(false);
    }
  };

  const handleFetchUsers = () => {
    fetchUsers();
  };

  const handleAddFriend = async (userId: string) => {
    try {
      const db = getFirestore();
      const userDoc = doc(db, 'users', userId);
      // Предположим, что у вас есть поле "friends", которое представляет список друзей
      // Вы можете добавить логику, соответствующую вашей структуре данных Firebase
      await setDoc(userDoc, { friends: true }, { merge: true });
      // Обновляем список пользователей после добавления друга
      fetchUsers();
    } catch (error) {
      console.error('Error adding friend:', error);
    }
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.fetchButton}
        onPress={handleFetchUsers}
        disabled={isLoading || isFetched} // Отключаем кнопку после загрузки данных
      >
        <Text style={styles.fetchButtonText}>{isLoading ? 'Loading...' : 'Fetch Users'}</Text>
      </TouchableOpacity>
      {isFetched && users.length > 0 && ( // Показываем список только если данные загружены и не пусты
        <ScrollView contentContainerStyle={styles.scrollViewContainer}>
          {users.map(user => (
            <TouchableOpacity
              key={user.id}
              style={styles.userCard}
              onPress={() => handleAddFriend(user.id)}
            >
              <Image source={require('../../img/profile.png')} style={styles.userImage} />
              <Text style={styles.userName}>{user.name}</Text>
            </TouchableOpacity>
          ))}
        </ScrollView>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#480066',
    justifyContent: 'center',
    alignItems: 'center',
  },
  fetchButton: {
    backgroundColor: '#007AFF',
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 5,
    marginBottom: 20,
  },
  fetchButtonText: {
    color: '#FFFFFF',
    fontSize: 16,
  },
  scrollViewContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    paddingVertical: 10,
  },
  userCard: {
    width: '45%',
    backgroundColor: '#FFFFFF',
    borderRadius: 8,
    alignItems: 'center',
    marginBottom: 10,
    elevation: 2,
  },
  userImage: {
    width: 100,
    height: 100,
    borderRadius: 50,
    marginVertical: 10,
  },
  userName: {
    color: '#000000',
    fontSize: 16,
    marginBottom: 10,
  },
});

export default AddFriendsScreen;
