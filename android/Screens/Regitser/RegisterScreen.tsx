import React, { useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet, ScrollView, ActivityIndicator, Alert } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { Picker } from '@react-native-picker/picker';
import { initializeApp } from "firebase/app";
import { getFirestore, collection, addDoc } from "firebase/firestore";
import { getAuth } from 'firebase/auth';

const RegisterScreen = () => {
  const [email, setEmail] = useState('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [gender, setGender] = useState('');
  const [preferences, setPreferences] = useState('');
  const [age, setAge] = useState('');
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');
  const [invalidAge, setInvalidAge] = useState(false); // Добавляем состояние для недействительного возраста

  const navigation = useNavigation();

  function generateUserId() {
    const timestamp = new Date().getTime();
    const randomNumber = Math.floor(Math.random() * 10000);
    const userId = `${timestamp}-${randomNumber}`;
    return userId;
  }

  const handleRegister = async () => {
    try {
      setLoading(true);

      if (!validateAge()) {
        setInvalidAge(true);
        return;
      }

      const db = getFirestore();
      const usersCollection = collection(db, 'users');
      const userId = generateUserId();

      const userRef = await addDoc(usersCollection, {
        userId,
        username,
        email,
        phoneNumber,
        password,
        gender,
        preferences,
        age: Number(age)
      });

      console.log('User registered successfully:', userRef.id);

      navigation.navigate('Home'as never);
    } catch (error) {
      setError('An error occurred during registration. Please try again.');
      console.error('Registration error:', error);
      Alert.alert('Error', 'An error occurred during registration. Please try again.');
    } finally {
      setLoading(false);
    }
  };

  const goToLogin = () => {
    navigation.navigate('Login'as never);
  };

  const goToAgreement = () => {
    // Implement agreement screen navigation here
  };

  const validateAge = () => {
    const parsedAge = parseInt(age);
    return !isNaN(parsedAge) && parsedAge >= 18 && parsedAge <= 100;
  };

  return (
    <ScrollView contentContainerStyle={styles.scrollContainer}>
      <View style={styles.container}>
        <Text style={styles.title}>Регистрация</Text>
        {error ? <Text style={styles.errorText}>{error}</Text> : null}
        <TextInput
          style={styles.input}
          placeholder="Email"
          value={email}
          onChangeText={setEmail}
        />
        <TextInput
          style={styles.input}
          placeholder="Username"
          value={username}
          onChangeText={setUsername}
        />
        <TextInput
          style={styles.input}
          placeholder="Пароль"
          secureTextEntry
          value={password}
          onChangeText={setPassword}
        />
        <View style={styles.input}>
          <TextInput
            placeholder="Номер телефона"
            keyboardType="phone-pad"
            value={phoneNumber}
            onChangeText={setPhoneNumber}
          />
        </View>
        <Picker
          style={styles.input}
          selectedValue={gender}
          onValueChange={(itemValue, itemIndex) => setGender(itemValue)}
        >
          <Picker.Item label="Выберите пол" value="" />
          <Picker.Item label="Мужчина" value="male" />
          <Picker.Item label="Женщина" value="female" />
        </Picker>
        <Picker
          style={styles.input}
          selectedValue={age}
          onValueChange={(itemValue, itemIndex) => setAge(itemValue)}
        >
          <Picker.Item label="Выберите возраст" value="" />
          {Array.from({ length: 100 - 18 + 1 }, (_, i) => (
            <Picker.Item key={18 + i} label={`${18 + i} лет`} value={`${18 + i}`} />
          ))}
        </Picker>
        {invalidAge && <Text style={styles.errorText}>Введите корректный возраст от 18 до 100 лет.</Text>}
        <TextInput
          style={styles.input}
          placeholder="О себе"
          value={preferences}
          onChangeText={setPreferences}
        />
        <TouchableOpacity style={styles.registerButton} onPress={handleRegister}>
          <Text style={styles.registerButtonText}>Зарегистрироваться</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={goToLogin}>
          <Text style={styles.loginLink}>Уже есть аккаунт? Войти</Text>
        </TouchableOpacity>
        <Text style={styles.agreementText}>
          Нажимая кнопку "Зарегистрироваться", вы соглашаетесь с <TouchableOpacity onPress={goToAgreement}><Text style={styles.agreementLink}>пользовательским соглашением</Text></TouchableOpacity>.
        </Text>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  scrollContainer: {
    flexGrow: 1,
  },
  container: {
    flex: 1,
    backgroundColor: '#381e49',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
    color: '#fff',
    textAlign: 'center',
  },
  input: {
    width: '100%',
    backgroundColor: '#fff',
    marginBottom: 10,
    padding: 15,
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  registerButton: {
    backgroundColor: '#FF0000',
    paddingVertical: 15,
    paddingHorizontal: 50,
    borderRadius: 10,
    marginBottom: 10,
  },
  registerButtonText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  loginLink: {
    color: '#FF0000',
    textDecorationLine: 'underline',
    fontSize: 14,
    marginBottom: 10,
  },
  agreementText: {
    color: '#fff',
    textAlign: 'center',
    marginTop: 10,
  },
  agreementLink: {
    color: '#FF0000',
    textDecorationLine: 'underline',
  },
  errorText: {
    color: 'red',
    marginBottom: 10,
  },
});

export default RegisterScreen;
