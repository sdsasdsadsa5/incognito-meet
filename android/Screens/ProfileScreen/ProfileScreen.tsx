import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Animated } from 'react-native';
import { RouteProp, useNavigation } from '@react-navigation/native';
import { getFirestore, doc, setDoc } from 'firebase/firestore';

// Определение типа параметров маршрута
type ProfileScreenParams = {
  userName: string;
};

// Define the type for route params
type ProfileScreenRouteProp = RouteProp<{ params: ProfileScreenParams }, 'params'>;

// Define the ProfileScreen component
const ProfileScreen: React.FC<{ route: ProfileScreenRouteProp }> = ({ route }) => {
  const { userName } = route.params;
  const navigation = useNavigation(); // Получение объекта навигации

  // Анимация для имени пользователя
  const animatedValue = React.useRef(new Animated.Value(0)).current;

  React.useEffect(() => {
    Animated.timing(animatedValue, {
      toValue: 1,
      duration: 1000,
      useNativeDriver: true,
    }).start();
  }, [animatedValue]);

  const handleAddFriend = async () => {
    try {
      const db = getFirestore();
      const userDoc = doc(db, 'users', userName);
      // Предположим, что у вас есть поле "friends", которое представляет список друзей
      // Вы можете добавить логику, соответствующую вашей структуре данных Firebase
      await setDoc(userDoc, { friends: true }, { merge: true });
      // Возвращаемся на предыдущий экран
      navigation.goBack();
    } catch (error) {
      console.error('Error adding friend:', error);
    }
  };

  const handleMessage = () => {
    // Навигация на экран чата и передача параметра с именем пользователя
    console.log("Navigating to Chat screen with userName:", userName);
    navigation.navigate('Chat', { userName: userName });
  };

  return (
    <View style={styles.container}>
      <Animated.Text style={[styles.userName, { opacity: animatedValue }]}>{userName}</Animated.Text>
      <TouchableOpacity 
        style={[styles.button, styles.addButton]}
        onPress={handleAddFriend}
        activeOpacity={0.7}
      >
        <Text style={styles.buttonText}>Добавить в друзья</Text>
      </TouchableOpacity>
      <TouchableOpacity 
        style={[styles.button, styles.messageButton]}
        onPress={handleMessage}
        activeOpacity={0.7}
      >
        <Text style={styles.buttonText}>Написать сообщение</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
  },
  userName: {
    fontSize: 24,
    marginBottom: 20,
  },
  button: {
    padding: 10,
    borderRadius: 5,
    marginBottom: 10,
    minWidth: 200,
    alignItems: 'center',
    justifyContent: 'center',
  },
  addButton: {
    backgroundColor: '#007AFF',
  },
  messageButton: {
    backgroundColor: '#4CAF50',
  },
  buttonText: {
    color: '#FFFFFF',
    fontSize: 16,
  },
});

export default ProfileScreen;
