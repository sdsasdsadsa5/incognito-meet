import React, { useState, useEffect } from 'react';
import { View, Text, TextInput, TouchableOpacity, FlatList, StyleSheet } from 'react-native';
import { getFirestore, collection, addDoc, onSnapshot, doc, deleteDoc } from 'firebase/firestore';

interface Message {
  id: string;
  text: string;
  senderId: string;
  senderName: string;
}

const Chat = ({ currentUser }: { currentUser: { userId: string; username: string } }) => {
  const [messages, setMessages] = useState<Message[]>([]);
  const [newMessage, setNewMessage] = useState('');
  const [onlineUsers, setOnlineUsers] = useState<string[]>([]);
  const db = getFirestore();

  useEffect(() => {
    const unsubscribeMessages = subscribeToMessages();
    const unsubscribeOnlineUsers = subscribeToOnlineUsers();
    return () => {
      unsubscribeMessages();
      unsubscribeOnlineUsers();
    };
  }, []);

  const subscribeToMessages = () => {
    return onSnapshot(collection(db, 'messages'), (snapshot) => {
      const messagesData: Message[] = [];
      snapshot.forEach((doc) => {
        messagesData.push(doc.data() as Message);
      });
      setMessages(messagesData);
    });
  };

  const subscribeToOnlineUsers = () => {
    const onlineUsersRef = collection(db, 'onlineUsers');
    return onSnapshot(onlineUsersRef, (snapshot) => {
      const users: string[] = [];
      snapshot.forEach((doc) => {
        users.push(doc.id);
      });
      setOnlineUsers(users);
    });
  };

  const sendMessage = async () => {
    try {
      await addDoc(collection(db, 'messages'), {
        text: newMessage,
        senderId: currentUser ? currentUser.userId : 'anonymous',
        senderName: currentUser ? currentUser.username : 'Anonymous',
      });
      console.log('Message sent successfully');
      setNewMessage('');
    } catch (error) {
      console.error('Error sending message:', error);
    }
  };

  return (
    <View style={styles.container}>
      <FlatList
        data={messages}
        renderItem={({ item }) => (
          <View style={styles.message}>
            <Text style={styles.senderName}>{item.senderName}</Text>
            <Text>{item.text}</Text>
          </View>
        )}
        keyExtractor={(item) => item.id}
      />
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.input}
          value={newMessage}
          onChangeText={(text) => setNewMessage(text)}
          placeholder="Type your message..."
        />
        <TouchableOpacity style={styles.sendButton} onPress={sendMessage}>
          <Text>Send</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: '#f0f0f0',
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    padding: 10,
    borderRadius: 5,
    marginBottom: 10,
  },
  input: {
    flex: 1,
    borderWidth: 1,
    borderColor: '#cccccc',
    borderRadius: 5,
    paddingHorizontal: 10,
    marginRight: 10,
  },
  sendButton: {
    backgroundColor: '#0088cc',
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 5,
  },
  message: {
    padding: 10,
    backgroundColor: '#ffffff',
    borderRadius: 5,
    marginBottom: 10,
  },
  senderName: {
    fontWeight: 'bold',
    marginBottom: 5,
    color: '#0088cc',
  },
});

export default Chat;
