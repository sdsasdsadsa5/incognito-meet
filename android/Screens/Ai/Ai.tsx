import React, { Component } from 'react';
import { GiftedChat, IMessage } from 'react-native-gifted-chat'; // Добавляем IMessage из react-native-gifted-chat
import { ChatGPT } from './ChatGPT/ChatGPT';

interface State {
  messages: IMessage[]; // Определяем тип состояния для сообщений
}

export default class Ai extends Component<{}, State> {
  state: State = {
    messages: [],
  };

  componentDidMount() {
    this.setState({
      messages: [
        {
          _id: 1,
          text: 'Привет! Я готов вести беседу с вами.',
          createdAt: new Date(),
          user: {
            _id: 2,
            name: 'ChatGPT',
          },
        },
      ],
    });
  }

  onSend(messages: IMessage[] = []) {
    // Проверяем, что есть сообщение для отправки
    if (messages.length === 0 || !messages[0].text) {
      return;
    }
  
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, messages),
    }));
  
    // Отправка сообщения на обработку GPT и получение ответа
    const userMessage = messages[0].text;
    ChatGPT.getResponse(userMessage).then(response => {
      const newMessage = {
        _id: Math.random().toString(36).substring(7),
        text: response,
        createdAt: new Date(),
        user: {
          _id: 2,
          name: 'ChatGPT',
        },
      };
      this.setState(previousState => ({
        messages: GiftedChat.append(previousState.messages, [newMessage]),
      }));
    });
  }

  render() {
    return (
      <GiftedChat
        messages={this.state.messages}
        onSend={messages => this.onSend(messages)}
        user={{
          _id: 1,
        }}
      />
    );
  }
}
