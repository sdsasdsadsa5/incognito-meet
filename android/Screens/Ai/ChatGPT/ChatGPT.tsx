import axios from 'axios';

const openaiApiKey: string = 'sk-proj-KZUn5XU3EOemSnxW4Zp2T3BlbkFJTXdBoupXQCxg8lFmdc3X';

const ChatGPT = {
  async getResponse(userMessage: string) {
    try {
      const response = await axios.post(
        'https://api.openai.com/v1/chat/completions',
        {
          model: 'gpt-3.5-turbo',
          messages: [{ role: 'user', content: userMessage }],
        },
        {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${openaiApiKey}`,
          },
        }
      );

      const chatGptResponse = response.data.choices[0].message.content;

      return chatGptResponse;
    } catch (error) {
      console.error('Error fetching response from GPT:', error);
      return 'Извините, что-то пошло не так. Пожалуйста, попробуйте еще раз.';
    }
  }
};

export { ChatGPT };
