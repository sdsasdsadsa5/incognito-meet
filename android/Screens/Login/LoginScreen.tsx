import React, { useState } from 'react';
import { Animated, Easing, StyleSheet, Text, TextInput, TouchableOpacity, useColorScheme, View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";
import { collection, query, where, getDocs } from 'firebase/firestore'
import { db } from '../../../Config/firebasefig';


const LoginScreen = () => {
  const isDarkMode = useColorScheme() === 'dark';
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const buttonScale = new Animated.Value(1);
  const navigation = useNavigation();
  const [username, setUsername] = useState(''); // Инициализация переменной `username`

  const login = async (username:string, password:string) => {
    try {
        const usersRef = collection(db, 'users');
        const q = query(usersRef, where('username', '==', username));
        const querySnapshot = await getDocs(q);

        if (!querySnapshot.empty) {
            const user = querySnapshot.docs[0].data();

            // Проверяем соответствие введенного пароля хэшированному паролю в Firestore
            if (user.password === password) {
                return true; // Возвращаем true в случае успешного входа
            } else {
                throw new Error('Неверный пароль.');
            }
        } else {
            throw new Error('Пользователь с указанным именем пользователя не найден.');
        }
    } catch (error) {
        throw error;
    }
};

  
  const handleLogin = async () => {
    try {
      // Добавляем анимацию при нажатии на кнопку входа
      Animated.sequence([
        Animated.timing(buttonScale, {
          toValue: 0.9,
          duration: 50,
          easing: Easing.ease,
          useNativeDriver: true,
        }),
        Animated.timing(buttonScale, {
          toValue: 1,
          duration: 50,
          easing: Easing.ease,
          useNativeDriver: true,
        }),
      ]).start();
  
      // Вызываем функцию для аутентификации пользователя
      await login(username, password); // Используем username и password
  
      // Успешный вход, перенаправляем пользователя на домашнюю страницу
      navigation.navigate('Home' as never);
    } catch (error) {
      // Обработка ошибок входа пользователя
      console.error('Ошибка входа:', error);
      // Обработка ошибок, например, отображение сообщения об ошибке для пользователя
    }
  };
  

  const goToRegister = () => {
    navigation.navigate('Register'as never);
  };

  return (
    <View style={[styles.container, { backgroundColor: '#381e49' }]}>
      <Text style={[styles.title, { color: '#ff0000' }]}>incoGnito Mi MEET</Text>
      <View style={styles.inputContainer}>
      <TextInput
  style={[styles.input, { backgroundColor: isDarkMode ? '#4a235a' : '#ffffff', color: isDarkMode ? '#ffffff' : '#381e49' }]}
  placeholder="Username" // Placeholder изменен на "Username"
  placeholderTextColor={isDarkMode ? '#cccccc' : '#666666'}
  value={username} // Переменная `username` используется для значения
  onChangeText={setUsername} // `setUsername` используется для обновления значения `username`
/>

        <TextInput
          style={[styles.input, { backgroundColor: isDarkMode ? '#4a235a' : '#ffffff', color: isDarkMode ? '#ffffff' : '#381e49' }]}
          placeholder="Password"
          secureTextEntry={true}
          placeholderTextColor={isDarkMode ? '#cccccc' : '#666666'}
          value={password}
          onChangeText={setPassword}
        />
      </View>
      <Animated.View style={[styles.loginButtonContainer, { transform: [{ scale: buttonScale }] }]}>
        <TouchableOpacity style={[styles.loginButton, { backgroundColor: isDarkMode ? '#ffffff' : '#ff4d4d' }]} onPress={handleLogin}>
          <Text style={[styles.loginButtonText, { color: isDarkMode ? '#ff4d4d' : '#ffffff' }]}>Login</Text>
        </TouchableOpacity>
      </Animated.View>
      <TouchableOpacity onPress={goToRegister}>
        <Text style={{ color: '#ff0000', marginTop: 20 }}>Not registered yet? Register here!</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  inputContainer: {
    width: '80%',
    marginBottom: 20,
  },
  input: {
    height: 50,
    borderColor: 'transparent',
    borderWidth: 1,
    borderRadius: 10,
    paddingHorizontal: 20,
    marginBottom: 10,
    width: '100%',
  },
  loginButtonContainer: {
    alignItems: 'center',
  },
  loginButton: {
    paddingVertical: 15,
    paddingHorizontal: 80,
    borderRadius: 10,
  },
  loginButtonText: {
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

export default LoginScreen;
