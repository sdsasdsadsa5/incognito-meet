interface Translation {
  [key: string]: {
    [key: string]: string;
  };
}

const translationt: Translation = {
  en: {
    profile: 'Profile',
    folders: 'Folders',
    anonymity: 'Anonymity',
    // Другие переводы для английского языка
  },
  fr: {
    profile: 'Profil',
    folders: 'Dossiers',
    anonymity: 'Anonymat',
    // Другие переводы для французского языка
  },
  ru: {
    profile: 'Профиль',
    folders: 'Папки',
    anonymity: 'Анонимность',
    // Другие переводы для русского языка
  },
  ky: {
    profile: 'Профиль', // Кыргызский перевод
    folders: 'Файлдар', // Кыргызский перевод
    anonymity: 'Анонимдик', // Кыргызский перевод
    // Другие переводы для кыргызского языка
  },
  kk: {
    profile: 'Профиль', // Казахский перевод
    folders: 'Қалтар', // Казахский перевод
    anonymity: 'Анонимдік', // Казахский перевод
    // Другие переводы для казахского языка
  },
  zh: {
    profile: '个人资料', // Китайский перевод
    folders: '文件夹', // Китайский перевод
    anonymity: '匿名性', // Китайский перевод
    // Другие переводы для китайского языка
  },
  // Добавьте переводы для других языков по аналогии
  // Добавим переводы для некоторых других языков
  de: {
    profile: 'Profil', // Немецкий перевод
    folders: 'Ordner', // Немецкий перевод
    anonymity: 'Anonymität', // Немецкий перевод
    // Другие переводы для немецкого языка
  },
  es: {
    profile: 'Perfil', // Испанский перевод
    folders: 'Carpetas', // Испанский перевод
    anonymity: 'Anonimato', // Испанский перевод
    // Другие переводы для испанского языка
  },
  it: {
    profile: 'Profilo', // Итальянский перевод
    folders: 'Cartelle', // Итальянский перевод
    anonymity: 'Anonimato', // Итальянский перевод
    // Другие переводы для итальянского языка
  },
  pt: {
    profile: 'Perfil', // Португальский перевод
    folders: 'Pastas', // Португальский перевод
    anonymity: 'Anonimato', // Португальский перевод
    // Другие переводы для португальского языка
  },
  // И так далее...
};

export default translationt;
