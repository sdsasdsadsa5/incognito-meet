import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, Switch, StyleSheet, Alert, ScrollView } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { Picker } from '@react-native-picker/picker';
import iso6391 from 'iso-639-1';
import { signOut, getAuth } from 'firebase/auth';
import { activatePremium, deactivatePremium } from '../api';

const Settings = () => {
  const navigation = useNavigation();
  const [anonymityEnabled, setAnonymityEnabled] = useState(false);
  const [selectedLanguage, setSelectedLanguage] = useState('en');
  const [darkModeEnabled, setDarkModeEnabled] = useState(false);
  const [premiumEnabled, setPremiumEnabled] = useState(false);

  // Функция для включения/выключения анонимности
  const toggleAnonymity = () => {
    setAnonymityEnabled(prevState => !prevState);
  };

  // Функция для включения/выключения темного режима
  const toggleDarkMode = () => {
    setDarkModeEnabled(prevState => !prevState);
  };

  // Функция для включения/выключения премиума
  const togglePremium = async () => {
    try {
      if (!premiumEnabled) {
        await activatePremium(); // Активация премиума через API
        setPremiumEnabled(true); // Установка флага премиума в true
      } else {
        await deactivatePremium(); // Деактивация премиума через API
        setPremiumEnabled(false); // Установка флага премиума в false
      }
    } catch (error) {
      console.error('Error toggling premium:', error);
      // Обработка ошибки при включении/выключении премиума
      Alert.alert('Error', 'An error occurred while toggling premium.');
    }
  };

  // Получение списка языков для Picker
  const languageCodes = iso6391.getAllCodes();
  const languages = languageCodes.map(code => ({
    label: iso6391.getName(code),
    value: code,
  }));

  // Обработчик изменения выбранного языка
  const changeLanguage = (newLanguage: string) => {
    setSelectedLanguage(newLanguage);
    // Здесь можно добавить логику для изменения языка интерфейса
  };

  // Функция для выхода из аккаунта
  const handleLogout = () => {
    Alert.alert(
      'Logout',
      'Are you sure you want to log out?',
      [
        {
          text: 'Cancel',
          style: 'cancel',
        },
        {
          text: 'Logout',
          onPress: async () => {
            try {
              const auth = getAuth();
              await signOut(auth); // Выход из аккаунта
              navigation.navigate('Login'as never); // Переход на экран входа
            } catch (error) {
              console.error('Error signing out:', error);
              // Обработка ошибки при выходе из аккаунта
              Alert.alert('Error', 'An error occurred while signing out.');
            }
          },
        },
      ],
      { cancelable: false }
    );
  };

  // Добавление функции создания чата
  

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <Text style={styles.title}>Settings</Text>
      {/* Навигация к профилю */}
      <TouchableOpacity onPress={() => navigation.navigate('Profile'as never)} style={styles.option}>
        <Text style={styles.optionText}>Profile</Text>
      </TouchableOpacity>
      {/* Навигация к папкам */}
      <TouchableOpacity onPress={() => navigation.navigate('Folders'as never)} style={styles.option}>
        <Text style={styles.optionText}>Folders</Text>
      </TouchableOpacity>
      {/* Переключатель для анонимности */}
      
      {/* Переключатель для премиума */}
      <View style={styles.option}>
        <Text style={styles.optionText}>Premium</Text>
        <Switch value={premiumEnabled} onValueChange={togglePremium} />
      </View>
      {/* Переключатель для темного режима */}
      <View style={styles.option}>
        <Text style={styles.optionText}>Dark Mode</Text>
        <Switch value={darkModeEnabled} onValueChange={toggleDarkMode} />
      </View>
      {/* Выбор языка */}
      <Text style={styles.subtitle}>Languages:</Text>
      <View style={styles.pickerContainer}>
        <Picker
          selectedValue={selectedLanguage}
          onValueChange={(itemValue, itemIndex) => changeLanguage(itemValue)}
          style={styles.picker}
        >
          {languages.map(language => (
            <Picker.Item key={language.value} label={language.label} value={language.value} />
          ))}
        </Picker>
      </View>
      
      <TouchableOpacity onPress={handleLogout} style={styles.logoutButton}>
        <Text style={styles.logoutButtonText}>Logout</Text>
      </TouchableOpacity>
      {/* Версия приложения */}
      <Text style={styles.versionText}>INCOGNITO MEET beta v 1.0.0</Text>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: '#480066', // Темно-фиолетовый цвет фона
    paddingVertical: 20,
    paddingHorizontal: 20,
  },
  title: {
    fontSize: 28,
    fontWeight: 'bold',
    marginBottom: 20,
    color: '#FFFFFF', // Белый цвет текста
    textAlign: 'center',
  },
  subtitle: {
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 20,
    marginBottom: 10,
    color: '#FFFFFF', // Белый цвет текста
  },
  option: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#CCCCCC',
  },
  optionText: {
    fontSize: 18,
    color: '#FFFFFF', // Белый цвет текста
  },
  pickerContainer: {
    borderWidth: 1,
    borderColor: '#CCCCCC',
    borderRadius: 8,
    marginTop: 10,
  },
  picker: {
    height: 50,
    color: '#FFFFFF', // Белый цвет текста
  },
  logoutButton: {
    backgroundColor: '#6a5acd', // Темно-фиолетовый цвет кнопки "Logout"
    borderRadius: 8,
    paddingVertical: 15,
    marginTop: 20,
  },
  logoutButtonText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#FFFFFF', // Белый цвет текста
    textAlign: 'center',
  },
  versionText: {
    fontSize: 14,
    color: '#FFFFFF', // Белый цвет текста
    textAlign: 'center',
    marginTop: 20,
  },
});

export default Settings;
