import React, { createContext, useContext, useState, ReactNode } from 'react';

interface Settings {
  anonymityEnabled: boolean;
  premiumEnabled: boolean;
  appColor: string;
  selectedLanguage: string;
  notificationEnabled: boolean;
  darkModeEnabled: boolean;
}

interface SettingsProviderProps {
  children: ReactNode; // Define children prop
}

const defaultSettings: Settings = {
  anonymityEnabled: false,
  premiumEnabled: false,
  appColor: 'purple',
  selectedLanguage: 'english',
  notificationEnabled: false,
  darkModeEnabled: false,
};

const SettingsContext = createContext<{
  settings: Settings;
  updateSettings: React.Dispatch<React.SetStateAction<Settings>>;
}>({
  settings: defaultSettings,
  updateSettings: () => {},
});

export const useSettings = () => useContext(SettingsContext);

export const SettingsProvider: React.FC<SettingsProviderProps> = ({ children }) => { // Specify props type
  const [settings, setSettings] = useState<Settings>(defaultSettings);

  const updateSettings: React.Dispatch<React.SetStateAction<Settings>> = (newSettings) => {
    setSettings((prevSettings) => ({ ...prevSettings, ...newSettings }));
  };

  return (
    <SettingsContext.Provider value={{ settings, updateSettings }}>
      {children}
    </SettingsContext.Provider>
  );
};
