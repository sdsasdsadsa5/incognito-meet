import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';

const Sticker = ({ sticker }) => {
  return (
    <View style={styles.stickerContainer}>
      <Image source={sticker.source} style={styles.stickerImage} />
      <Text style={styles.stickerText}>{sticker.text}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  stickerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    backgroundColor: '#f0f0f0',
    borderRadius: 10,
    marginBottom: 5,
  },
  stickerImage: {
    width: 50,
    height: 50,
    marginRight: 10,
  },
  stickerText: {
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default Sticker;
