import { getAuth, createUserWithEmailAndPassword } from "firebase/auth";
import { getFirestore, collection, addDoc } from 'firebase/firestore';

const register = async (email: string, password: string, username: string, phoneNumber: string, gender: string, preferences: string, age: string): Promise<boolean> => {
  try {
    // Получаем экземпляры Firebase Auth и Firestore
    const firebaseAuth = getAuth();
    const firestore = getFirestore();

    // Регистрация пользователя через Firebase Authentication
    await createUserWithEmailAndPassword(firebaseAuth, email, password);
    
    // После успешной регистрации сохраняем данные пользователя в Firestore
    const usersCollection = collection(firestore, 'users');
    await addDoc(usersCollection, {
      username,
      email,
      phoneNumber,
      gender,
      preferences,
      age: Number(age)
    });

    return true; // Возвращаем true в случае успешной регистрации
  } catch (error) {
    throw error; // Бросаем любые ошибки
  }
};

export default register;
