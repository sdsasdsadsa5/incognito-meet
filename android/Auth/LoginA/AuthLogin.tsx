// Import the functions you need from the SDKs you need
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";

// Функция для входа пользователя
const login = async (email: string, password: string) => {
  try {
    const auth = getAuth(); // Получаем аутентификацию из уже инициализированного Firebase
    await signInWithEmailAndPassword(auth, email, password);
    return true; // Возвращаем true в случае успешного входа
  } catch (error) {
    throw error; // В случае ошибки выбрасываем исключение
  }
};

export default login;
