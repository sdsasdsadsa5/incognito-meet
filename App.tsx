import React, { Component } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from './android/Screens/Login/LoginScreen';
import RegisterScreen from './android/Screens/Regitser/RegisterScreen';

import { initializeApp } from 'firebase/app';

import Home from './android/Screens/Home/Home';
import Chat from './android/Screens/Chat1/Chat';
import Settings from './android/Screens/Settings/Settings';
import ProfileScreen from './android/Screens/ProfileScreen/ProfileScreen';
import { firebaseConfig } from './Config/firebasefig';
import AddFriendsScreen from './android/Screens/Friends/Friends';
import Profile from './android/Screens/Profile/Profile';
import Ai from './android/Screens/Ai/Ai';









// Используем конфигурацию из импортированного файла
const app = initializeApp(firebaseConfig);





const Stack = createStackNavigator();

export default class App extends Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Login" component={LoginScreen} />
          <Stack.Screen name="Register" component={RegisterScreen} />
          <Stack.Screen name="Home" component={Home} />
          <Stack.Screen name="Chat" component={Chat} options={{ title: 'Чат' }} />
          <Stack.Screen name="Settings" component={Settings} />
          <Stack.Screen name="AddFriends" component={AddFriendsScreen} />
          <Stack.Screen name="ProfileScreen" component={ProfileScreen} />
          <Stack.Screen name="Profile" component={Profile} />
          <Stack.Screen name="Ai" component={Ai} />


          

        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}